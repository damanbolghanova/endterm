package main

import (
	"com.grpc.tleu/CalculatorService/proto"
	"com.grpc.tleu/greet/greetpb"
	"context"
	"fmt"
	"io"
)

import (
	"google.golang.org/grpc"
	"log"
)

func main() {
	fmt.Println("Hello I'm a client")

	conn, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("could not connect: %v", err)
	}
	defer conn.Close()

	c := proto.NewCalculatorServiceClient(conn)
	//doPrimeNum(c)

	doAverage(c)
}

func doPrimeNum(c proto.CalculatorServiceClient) {
	stream, err := c.PrimeNumberDecomposition(context.Background(), &proto.PrimeNumRequest{
		PrimeNum: 120,
	})
	if err != nil {
		log.Fatalf("Error while open stream: %v", err.Error())
	}

	for {
		res, err := stream.Recv()
		if err == io.EOF {
			log.Println("Finished")
			return
		}
		if err != nil {
			log.Fatalf("Error: %v", err.Error())
		}
		log.Printf("Prime Number: %v", res.GetResult())
	}

}

func doAverage(c proto.CalculatorServiceClient) {
	stream, err := c.FindAverage(context.Background())
	if err != nil {
		log.Fatalf("Error: %v", err.Error())
	}

	requests := []*proto.AverageRequest{
		proto.AverageRequest{
			Number: 5,
		},
		proto.AverageRequest{
			Number: 2,
		},
		proto.AverageRequest{
			Number: 3,
		},
	}
}
