package main

import (
	"com.grpc.tleu/greet/greetpb"
	"context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/profiling/proto"
	"io"
	"log"
	"net"
)

type Server struct {
	proto.UnimplementedProfilingServer
}

func (s *Server) PrimeNumber(req *proto.PrimeNumRequest, stream proto.CalculatorService_PrimeNumberDecompositionServer) error {
	log.Println("PrimeNumberDecomposition")
	num := int32(2)
	input := req.GetPrimeNum.GetNumber()
	for input > 1 {
		if num%input == 0 {
			input = input / num
			err := stream.Send(&proto.PrimeNumResponce{
				Result: num,
			})
			if err != nil {
				log.Fatalf("Error while senfing to client: %v", err.Error())
				return err
			}
		} else {
			num++
		}
	}
	return nil
}

func (s *Server) FindAverage(stream proto.CalculatorService_FindAverageServer) error {
	log.Println("Finding Average")
	var res float32
	var count int
	for {
		req, err := stream.Recv()
		if err == io.EOF {
			return stream.SendAndClose(&proto.AverageResponse{
				Result: res / float32(count),
			})
		}
		if err != nil {
			log.Fatalf("Error: %v", err.Error())
		}
		res += req.GetNumber()
		count++
	}
}

func main() {
	l, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf("Failed to listen:%v", err)
	}

	s := grpc.NewServer()
	proto.RegisterCalculatorServiceServer(s, &Server{})
	log.Println("Server is running on port:50051")
	if err := s.Serve(l); err != nil {
		log.Fatalf("failed to serve:%v", err)
	}
}
